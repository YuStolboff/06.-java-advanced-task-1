#Practice

##Task 1

Create application with 3 threads producing random hotel booking requests (plain Java object with fields such as 'ad', 'date', 'hotel',

etc.) and 6 consuming threads emulating some activity for 5 seconds (sleep).

Use single queue (max capacity - five elements) for all threads, and wait()/notify() methods (implement your own queue).

All threads should log their state ('producer #A: sent XXX', 'booker #Z: received XXX', 'booker #Z: processed XXX').

Total number of generated requests: 15

Program should wait until last thread complete request processing.