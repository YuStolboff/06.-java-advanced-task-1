package com.epam.advancedJava.task1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Booker extends Thread {

    private BlockQueue<String> queue;
    private String name;
    private static final Logger log = LoggerFactory.getLogger(Booker.class);

    public Booker(BlockQueue<String> queue, String name) {
        this.queue = queue;
        this.name = name;
    }

    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                Thread.sleep(5000);
                if (this.queue.get() == null) {
                    Thread.currentThread().interrupt();
                    break;
                }
            } catch (InterruptedException e) {
                log.info("Booker: {}, {}", name, e);
            }
        }
    }
}
