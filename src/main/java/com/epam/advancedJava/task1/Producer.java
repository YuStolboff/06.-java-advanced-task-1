package com.epam.advancedJava.task1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Producer extends Thread {

    private BlockQueue<String> queue;
    private String name;
    private static final Logger log = LoggerFactory.getLogger(Producer.class);

    Producer(BlockQueue<String> queue, String name) {
        this.queue = queue;
        this.name = name;
    }

    public void run() {
        for (int i = 0; i < 5; i++) {
            try {
                queue.put(this.name);
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                log.info("Producer: {}, {}", name, e);
            }
        }
    }
}
