package com.epam.advancedJava.task1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.Queue;

public class BlockQueue<E> {

    private static final Logger log = LoggerFactory.getLogger(BlockQueue.class);

    private Queue<E> queue = new LinkedList<>();
    private int capacity;
    private int message;
    private volatile int count = 0;

    public BlockQueue(int capacity, int message) {
        this.capacity = capacity;
        this.message = message;
    }

    public synchronized void put(E element) throws InterruptedException {
        if (count < message) {
            while (queue.size() == capacity) {
                if (count == message) {
                    Thread.currentThread().interrupt();
                    return;
                } else {
                    wait();
                }
            }
            queue.add(element);
            log.info("Producer: {}, add hotel", Producer.currentThread());
            count++;
            log.info("Number of hotels: {}", count);
            notify();
        } else {
            Thread.currentThread().interrupt();
        }
    }

    public synchronized E get() throws InterruptedException {
        while (queue.isEmpty()) {
            if (count == message) {
                Thread.currentThread().interrupt();
                return null;
            } else {
                wait();
            }
        }
        E element = queue.remove();
        log.info("Booker: {}, take hotel", Booker.currentThread());
        notify();
        return element;
    }
}
