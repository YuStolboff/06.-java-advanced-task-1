package com.epam.advancedJava.task1;

import java.util.ArrayList;
import java.util.List;

public class Main {

    private static BlockQueue<String> queue = new BlockQueue<>(5, 15);
    private static List<Producer> producers = new ArrayList<>();
    private static List<Booker> bookers = new ArrayList<>();

    public static void main(String[] args) {
        for (int i = 0; i < 3; i++) {
            producers.add(new Producer(queue, " " + (i + 1)));
        }
        for (Producer producer : producers) {
            producer.start();
        }
        for (int i = 0; i < 6; i++) {
            bookers.add(new Booker(queue, " " + (i + 1)));
        }
        for (Booker booker : bookers) {
            booker.start();
        }
    }
}

